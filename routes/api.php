<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use App\Http\Controllers\ProjectController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\UserController;
//project api
Route::get('/gettest', [ProjectController::class, 'getApi']);
Route::post('/posttest', [ProjectController::class, 'postApi']);

//task api
Route::post('/gettask', [TaskController::class, 'getApi']);
Route::post('/posttask', [TaskController::class, 'postApi']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::middleware(['cors'])->group(function () {
//     //dropdown projects api
//    Route::get('/getproject', [ProjectController::class, 'getProject']);
// });


//dropdown projects api
 Route::get('/getproject', [ProjectController::class, 'getProject']);
//dropdown users api
 Route::get('/getusers', [UserController::class, 'getUsers']);
//Get project api 


// Route::get('/list',function($id){
//     $id=20;
//     return Http::get("https://fakestoreapi.com/products");
    //$id=20;
    //return Http::get("http://127.0.0.1:8000/products/$projectname");
   //return Http::get("http://192.168.0.255/products/$projectname");
// });

// Route::get('/get',function(){

// });

