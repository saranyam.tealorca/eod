<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TaskController;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('filter', function () {
    return view('details.filter');
});

Route::get('/getjson', [ProjectController::class, 'getJson']);

//login routes
Route::get('login', [AdminController::class, 'index'])->name('login');
Route::get('dashboard', [AdminController::class, 'dashboard'])->name('dashboard');
Route::post('index', [AdminController::class, 'adminLogin'])->name('login.post');

//admin routes
 Route::prefix('details')->middleware('auth')->group(function () {
Route::get('get_details',[ProjectController::class, 'get_details'])->name('project.export');
Route::get('getlist', [ProjectController::class, 'list'])->name('project.list');
Route::get('searchlist', [ProjectController::class, 'searchList'])->name('list.search');
Route::get('filter{serializeArray}', [ProjectController::class, 'filter'])->name('project.filter');
Route::get('logout', [AdminController::class, 'logout'])->name('logout');
Route::get('projectlist',[ProjectController::class, 'projectList'])->name('project.drop');
//projects crud routes
Route::get('create',[ProjectController::class, 'create'])->name('project.create');
Route::post('store',[ProjectController::class, 'store'])->name('project.store');
Route::get('index',[ProjectController::class, 'index'])->name('project.index');
Route::get('delete{id}',[ProjectController::class, 'destroy'])->name('project.delete');
Route::get('edit{id}',[ProjectController::class, 'edit'])->name('project.update');
//users crud routes
Route::get('view',[UserController::class, 'create'])->name('user.create');
Route::post('add',[UserController::class, 'store'])->name('user.store');
Route::get('list',[UserController::class, 'index'])->name('user.index');
Route::get('update{id}',[UserController::class, 'edit'])->name('user.update');
Route::get('destroy{id}',[UserController::class, 'destroy'])->name('user.delete');
Route::get('tasklist',[TaskController::class, 'index'])->name('task.list');

});





// Route::get('/users/export-filter', [ProjectController::class, 'filter'])->name('users.export-filter');


 