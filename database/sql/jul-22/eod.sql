-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 29, 2022 at 12:31 PM
-- Server version: 8.0.30-0ubuntu0.20.04.2
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eod`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `name`, `email`, `created_at`, `updated_at`) VALUES
(1, 'anitha', 'anitha@gmail.com', '2022-07-28 06:24:29', '2022-07-28 06:24:29'),
(2, 'saranya', 'saranya@gmail.com', '2022-07-28 06:54:41', '2022-07-28 06:54:41'),
(5, 'gopiraj', 'gopiraj@gmail.com', '2022-07-28 23:52:21', '2022-07-28 23:52:21'),
(6, 'gopikrishnan', 'gopi123@gmail.com', '2022-07-28 23:52:44', '2022-07-28 23:52:44');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_07_25_074809_create_project_table', 1),
(6, '2022_07_25_111931_create_task_table', 1),
(7, '2022_07_26_054123_add_project_id_to_task', 2),
(8, '2022_07_26_054412_create_task_table', 3),
(9, '2022_07_28_104959_create_employee_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` bigint UNSIGNED NOT NULL,
  `project_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extra_param` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `project_name`, `extra_param`, `created_at`, `updated_at`) VALUES
(1, 'Lwd Project', NULL, '2022-07-25 23:37:14', '2022-07-25 23:37:14'),
(2, 'Travel kit', NULL, '2022-07-25 23:37:14', '2022-07-25 23:37:14'),
(3, 'GRS kitchen', NULL, '2022-07-25 23:37:14', '2022-07-25 23:37:14'),
(4, 'GHEE Easy', NULL, '2022-07-25 23:37:14', '2022-07-25 23:37:14'),
(5, 'lienitnow', NULL, '2022-07-25 23:37:14', '2022-07-25 23:37:14'),
(6, 'EODstatus', NULL, '2022-07-25 23:37:14', '2022-07-25 23:37:14'),
(7, 'Playball app', NULL, '2022-07-27 08:41:04', '2022-07-22 08:41:15'),
(8, 'TJmarts', NULL, '2022-07-28 08:41:18', '2022-07-28 08:41:22'),
(9, 'Laravel snippet', NULL, '2022-07-28 03:00:05', '2022-07-28 05:17:11');

-- --------------------------------------------------------

--
-- Table structure for table `task`
--

CREATE TABLE `task` (
  `id` bigint UNSIGNED NOT NULL,
  `project_id` bigint UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateOfTheTask` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `taskName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `projectStatus` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extraParam` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `task`
--

INSERT INTO `task` (`id`, `project_id`, `name`, `dateOfTheTask`, `taskName`, `projectStatus`, `rating`, `extraParam`, `created_at`, `updated_at`) VALUES
(3, 8, 'Bava', '2022-07-26', 'cloning', 'Pending', '3', '{\"name\":\"Bava\",\"dateOfTheTask\":\"2022-07-26\",\"projectName\":\"Playball app\",\"taskName\":\"cloning\",\"projectStatus\":\"Pending\",\"rating\":\"3\"}', '2022-07-26 06:02:57', '2022-07-26 06:02:57'),
(4, 8, 'Bava', '2022-07-26', 'ticketing page', 'In Progress', '2', '{\"name\":\"Bava\",\"dateOfTheTask\":\"2022-07-26\",\"projectName\":\"TJmarts\",\"taskName\":\"ticketing page\",\"projectStatus\":\"In Progress\",\"rating\":\"2\"}', '2022-07-26 06:02:57', '2022-07-26 06:02:57'),
(10, 1, 'Anitha', '2022-07-27', 'slack interpretation', 'Pending', '2', '{\"name\":\"Anitha\",\"dateOfTheTask\":\"2022-07-27\",\"projectName\":\"TJmarts\",\"taskName\":\"slack interpretation\",\"projectStatus\":\"Pending\",\"rating\":\"2\"}', '2022-07-26 23:22:31', '2022-07-26 23:22:31'),
(11, 1, 'Anitha', '2022-07-27', 'transferred form data into google sheets', 'In Progress', '1', '{\"name\":\"Anitha\",\"dateOfTheTask\":\"2022-07-27\",\"projectName\":\"Playball app\",\"taskName\":\"transferred form data into google sheets\",\"projectStatus\":\"In Progress\",\"rating\":\"1\"}', '2022-07-26 23:22:31', '2022-07-26 23:22:31'),
(15, 2, 'Anitha', '2022-07-27', 'sdf', 'In Progress', '1', '{\"name\":\"Anitha\",\"dateOfTheTask\":\"2022-07-27\",\"projectName\":\"Lwd Project\",\"taskName\":\"sdf\",\"projectStatus\":\"In Progress\",\"rating\":\"1\"}', '2022-07-26 23:34:20', '2022-07-26 23:34:20'),
(19, 7, 'Bava', '2022-07-27', 'sdfsdf', 'Pending', '3', '{\"name\":\"Bava\",\"dateOfTheTask\":\"2022-07-27\",\"projectName\":\"Playball app\",\"taskName\":\"sdfsdf\",\"projectStatus\":\"Pending\",\"rating\":\"3\"}', '2022-07-27 00:29:38', '2022-07-27 00:29:38'),
(20, 5, 'Bava', '2022-07-27', 'ticketing page', 'Pending', '2', '{\"name\":\"Bava\",\"dateOfTheTask\":\"2022-07-27\",\"projectName\":\"lienitnow\",\"taskName\":\"ticketing page\",\"projectStatus\":\"Pending\",\"rating\":\"2\"}', '2022-07-27 00:29:38', '2022-07-27 00:29:38'),
(21, 1, 'saranya', '2022-07-27', 'test', 'completed', '2', '{\"projectName\":\"Lwd Project\",\"projectStatus\":\"completed\",\"taskName\":\"test\",\"name\":\"saranya\",\"dateOfTheTask\":\"26\\/07\\/22\",\"rating\":\"2\"}', '2022-07-28 02:13:24', '2022-07-28 02:13:24'),
(22, 9, 'Karthi', '2022-07-28', 'ticketing page', 'In Progress', '1', '{\"name\":\"Karthi\",\"dateOfTheTask\":\"2022-07-28\",\"projectName\":\"Laravel snippet\",\"taskName\":\"ticketing page\",\"projectStatus\":\"In Progress\",\"rating\":\"1\"}', '2022-07-28 05:36:32', '2022-07-28 05:36:32'),
(23, 8, 'Karthi', '2022-07-28', 'werwerwer', 'In Progress', '3', '{\"name\":\"Karthi\",\"dateOfTheTask\":\"2022-07-28\",\"projectName\":\"TJmarts\",\"taskName\":\"werwerwer\",\"projectStatus\":\"In Progress\",\"rating\":\"3\"}', '2022-07-28 05:36:32', '2022-07-28 05:36:32'),
(24, 5, 'saranya', '2022-07-28', 'sdf', 'Pending', '1', '{\"name\":\"saranya\",\"dateOfTheTask\":\"2022-07-28\",\"projectName\":\"lienitnow\",\"taskName\":\"sdf\",\"projectStatus\":\"Pending\",\"rating\":\"1\"}', '2022-07-28 07:29:19', '2022-07-28 07:29:19'),
(25, 8, 'saranya', '2022-07-29', 'slack interpretation', 'Pending', '2', '{\"name\":\"saranya\",\"dateOfTheTask\":\"2022-07-29\",\"projectName\":\"TJmarts\",\"taskName\":\"slack interpretation\",\"projectStatus\":\"Pending\",\"rating\":\"2\"}', '2022-07-29 01:12:28', '2022-07-29 01:12:28'),
(26, 9, 'saranya', '2022-07-29', 'transferred form data into google sheets', 'In Progress', '1', '{\"name\":\"saranya\",\"dateOfTheTask\":\"2022-07-29\",\"projectName\":\"Laravel snippet\",\"taskName\":\"transferred form data into google sheets\",\"projectStatus\":\"In Progress\",\"rating\":\"1\"}', '2022-07-29 01:12:28', '2022-07-29 01:12:28'),
(27, 8, 'saranya', '2022-07-29', 'ticketing page', 'In Progress', '5', '{\"name\":\"saranya\",\"dateOfTheTask\":\"2022-07-29\",\"projectName\":\"TJmarts\",\"taskName\":\"ticketing page\",\"projectStatus\":\"In Progress\",\"rating\":\"5\"}', '2022-07-29 01:14:50', '2022-07-29 01:14:50'),
(28, 6, 'saranya', '2022-07-29', 'sdfsdf', 'In Progress', '5', '{\"name\":\"saranya\",\"dateOfTheTask\":\"2022-07-29\",\"projectName\":\"EODstatus\",\"taskName\":\"sdfsdf\",\"projectStatus\":\"In Progress\",\"rating\":\"5\"}', '2022-07-29 01:14:50', '2022-07-29 01:14:50'),
(29, 9, 'saranya', '2022-07-29', 'fdfdfd', 'Completed', '5', '{\"name\":\"saranya\",\"dateOfTheTask\":\"2022-07-29\",\"projectName\":\"Laravel snippet\",\"taskName\":\"fdfdfd\",\"projectStatus\":\"Completed\",\"rating\":\"5\"}', '2022-07-29 01:14:50', '2022-07-29 01:14:50'),
(30, 8, 'saranya', '2022-07-29', 'fggfgf', 'Pending', '4', '{\"name\":\"saranya\",\"dateOfTheTask\":\"2022-07-29\",\"projectName\":\"TJmarts\",\"taskName\":\"fggfgf\",\"projectStatus\":\"Pending\",\"rating\":\"4\"}', '2022-07-29 01:14:50', '2022-07-29 01:14:50');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', NULL, '$2y$10$z0hLlejaT54Pu0WN78S59.sWJQQB2qGBRvdXkoe5iwPWqiFK0dhVi', NULL, '2022-07-28 00:51:22', '2022-07-28 00:51:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `task`
--
ALTER TABLE `task`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
