@extends('layouts.main')

@section('title')
   Add Projects
@endsection
 
@section('content')

<div class="container mt-3">
    @if ($message = session('success'))
    <div class="alert alert-success mx-1" role="alert">
        {{ $message }}
    </div>
     @endif
    <h2 class=" text-center"> Add Projects</h2> 
    @if (Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>
                    @endif
    <form id="login-form" action="{{ route('project.store') }}" method="post" role="form" style="display: block;">
            @csrf

                <div class="form-group">
                    <label>Project Name:</label>
                    <div class="input-group mb-3">
                    <input type="hidden" name="id" width="276" value="{{ old('id', (isset($project->id) ? $project->id : '')) }}"/>
                        <input type="text" id="project_name" name="project_name" width="276" value="{{ old('project_name', (isset($project->project_name) ? $project->project_name : '')) }}"/><br/>
                        @if ($errors->has('project_name'))
                                      <span class="text-danger">{{ $errors->first('project_name') }}</span>
                                  @endif
                    </div>
                </div> 
            
            <!-- <div class="form-group">
                    <label>Search Name</label>
                    <div class="input-group mb-3">
                        <input type="text" id="name"  name="name" width="276"/>
                    </div>
                </div>  -->
            
            <div class="form-group">
                    <div class="input-group mb-3">
                        <button type="submit" class="btn btn-primary">Add Project</button>
                    </div>
                </div>
            
            </form>
        </div> 
        @endsection