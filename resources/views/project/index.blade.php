@extends('layouts.main')

@section('title')
   Project list
@endsection
 
@section('content')
<div class="container mt-3">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Project Details</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('project.create') }}"> Add Project</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
<div class="card card-custom">
		<div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
               
            </div>
            <div class="card-toolbar">
                
            </div>
        </div>
		 <div class="card-body">
	<div class="row">
		<div class="col-12 pb-5">
			
			 <!-- <form action="" method="GET">
				<div class="row align-items-center">
	                    <div class="col-lg-9 col-xl-8">
	                        <div class="row align-items-center">
	                        	 <div class="col-md-6 my-2 my-md-0">

									<input type="search" name="q" class="form-control" placeholder="Search here" value="{{request('q')}}">
	                        	 </div>
	                        	 <div class="col-md-6 my-2 my-md-0">
	                        	 	<input type="submit" value="Search" class="btn btn-light-primary px-6 font-weight-bold"> &emsp;
									<a href="" class="refresh-btn btn btn-light-primary px-6 font-weight-bold">Reset</a>
	                        	 </div>
	                        </div>
	                    </div>
	             </div>
			</form> -->
				<br>
				@if(!empty($project->total()))

                        Showing Records {{ $project->firstItem() }} - {{ $project->lastItem() }} of {{ $project->total() }} (for page {{ $project->currentPage() }} )
                    @endif
			<table class="table table-bordered table-hover mt-3 table-striped border"  id="kt_datatable"> 
			  <thead>
			    <tr>
			      <th scope="col" class="text-center">#</th>
			      <th scope="col">Project Name</th>
			     
			      <th scope="col" class="custom-no-sort">Actions</th>
			    </tr>
			  </thead>
			  <tbody>
              @foreach ($project as $key=>$projects)
			        <tr class="border">
				      <th scope="row" class="text-center">
				      	{{($project->currentpage()-1) * $project->perpage() + $key + 1 }}
				      </th>
                      <td>{{ $projects->project_name }}</td>
				      <td class="custom-no-sort">
                      <a class="btn btn-primary" href="{{ route('project.update', ['id' => $projects->id]) }}">Edit</a>
                    <a class="btn btn-danger" href="{{ route('project.delete', ['id' => $projects->id]) }}">Delete</a>
				      </td>
				    </tr>
				  @endforeach
			  </tbody>
			</table>
			 <div class="row">
			<div class="col-12">
			{{$project->appends(Request::all())->links('pagination::bootstrap-4')}}
			</div>
			</div>
		</div>
		</div>
	</div>
</div>
</div>
    @endsection