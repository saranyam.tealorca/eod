@extends('layouts.main')

@section('title')
   Add Users
@endsection
 
@section('content')

<div class="container mt-3">
<div class="row">
        <div class="col-lg-12 margin-tb">
    @if ($message = session('success'))
    <div class="alert alert-success mx-1" role="alert">
        {{ $message }}
    </div>
     @endif
    <h2 class=" text-center"> Add Users</h2> 
    @if (Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('success') }}
                        </div>
                    @endif
     <div class="card card-custom">
	 <div class="card-header flex-wrap border-0 pt-6 pb-0">
     <div class="card-title">
     </div>
       <div class="card-toolbar">
                
            </div>
        </div>
		 <div class="card-body">
	<div class="row">

		<div class="col-12 pb-5">             
        <form id="login-form" action="{{ route('user.store') }}" method="post" role="form" style="display: block;">
            @csrf

                <div class="form-group">
                    <label>User Name:</label>
                    <div class="input-group mb-3">
                    <input type="hidden" name="id" width="276" value="{{ old('id', (isset($user->id) ? $user->id : '')) }}"/>
                        <input type="text" id="user_name" name="user_name" width="276" value="{{ old('user_name', (isset($user->name) ? $user->name : '')) }}"/><br/>
                        @if ($errors->has('user_name'))
                                      <span class="text-danger">{{ $errors->first('user_name') }}</span>
                                  @endif
                    </div>
                </div> 
                <div class="form-group">
                    <label>Email:</label>
                    <div class="input-group mb-3">
                  
                        <input type="email" id="email" name="email" width="276" value="{{ old('email', (isset($user->email) ? $user->email : '')) }}"/><br/>
                        @if ($errors->has('email'))
                                      <span class="text-danger">{{ $errors->first('email') }}</span>
                                  @endif
                    </div>
                </div> 
            <!-- <div class="form-group">
                    <label>Search Name</label>
                    <div class="input-group mb-3">
                        <input type="text" id="name"  name="name" width="276"/>
                    </div>
                </div>  -->
            
            <div class="form-group">
                    <div class="input-group mb-3">
                        <button type="submit" class="btn btn-primary">Add User</button>
                    </div>
                </div>
            
            </form>
            </div>
            </div>
        </div>
        </div>
        </div>
        </div> 
        @endsection