@extends('layouts.main')

@section('title')
Filters
@endsection
 
@section('content')

<div class="container mt-3 filter-container">
@if(($projects) || ($employees) )
@if ($message = session('success'))
    <div class="alert alert-success mx-1" role="alert">
        {{ $message }}
    </div>
     @endif
     @endif

    <h2 class=" text-center">Filters</h2> 
    <div class="form-center"> 
    <div class="form-group">
    <label>Select Filters:</label>
    <select class="form-control" id="size_select">
    <!-- <option value="">Choose filters</option> -->
    <option value="option1">Projects</option>
    <option value="option2">Users</option>
    <!-- <option value="option3">Date</option> -->
    <option value="option3">Task status</option>
   
    </select>
  </div>
</div>
 <div class="form-center">   
  
    <!--Size dropdown menu-->
   
          
 
            <div id="option1" class="size_chart">
            @if($projects)
           
              <form method="get" action="{{ route('project.export') }}">
            
           
           <div class="form-group">
                   <label>Start Date:</label>
                  
                   <input class="form-control" type="date" id="start_date" name="start_date" width="276" required/>
                  
               </div> 
               <div class="form-group">
                   <label>End Date:</label>
                   
                   <input class="form-control" type="date" id="end_date" name="end_date" width="276" required/>
               </div> 
               <!-- <div class="form-group">
                   <div class="input-group mb-3">
                       <button type="submit" class="btn btn-primary">Export By Date</button>
                   </div>
               </div>    -->
               
          
              <div class="form-group">
                    <label>Projects Name:</label>
                   
                   
                  
                        <!-- <input type="date" id="date" name="date" width="276" required/> -->
                        <select class="form-control" id="project_id" name="project_id">
                        @foreach ($projects as $project)
                        <option value="" selected disabled hidden>Choose Projects</option>
                        <option value="{{$project->id}}">{{$project->project_name}}</option>
                        @endforeach
                            </select>
                   
                </div> 
                <div class="form-group">
                 <div class="input-group mb-3">
                     <button type="submit" class="btn btn-primary">Export By Project</button>
                 </div>
             </div>   
           </form>
           @endif
            </div>
            
            <div id="option2" class="size_chart">
            @if($employees)
            
            <form method="get" action="{{ route('project.export') }}">
           

            <div class="form-group">
                   <label>Start Date:</label>
                  
                       <input class="form-control" type="date" id="start_date" name="start_date" width="276" required/>
                   </div>
               
               <div class="form-group">
                   <label>End Date:</label>
                  
                       <input class="form-control" type="date" id="end_date" name="end_date" width="276" required/>
                 
               </div> 

              <div class="form-group">
                    <label>Users Name:</label>
                   
                   
                  
                        <!-- <input type="date" id="date" name="date" width="276" required/> -->
                        <select class="form-control" id="user_name" name="user_name">
                        @foreach ($employees as $employee)
                        <option value="" selected disabled hidden>Choose Users</option>
                        <option value="{{$employee->name}}">{{$employee->name}}</option>
                        @endforeach
                            </select>
                   
                </div> 
                <div class="form-group">
                    <div class="input-group mb-3">
                        <button type="submit" class="btn btn-primary">Export By Name</button>
                    </div>
                </div>   
                
           </form>
           @endif
            </div>
            <!-- <div id="option3" class="size_chart">
            <form method="get" action="{{ route('project.export') }}">
           
            <div class="form-group">
                    <label>Start Date:</label>
                    <div class="input-group mb-3">
                        <input type="date" id="start_date" name="start_date" width="276" required/>
                    </div>
                </div> 
                <div class="form-group">
                    <label>End Date:</label>
                    <div class="input-group mb-3">
                        <input type="date" id="end_date" name="end_date" width="276" required/>
                    </div>
                </div> 
                <div class="form-group">
                    <div class="input-group mb-3">
                        <button type="submit" class="btn btn-primary">Export By Date</button>
                    </div>
                </div>   
                
           </form>
            </div> -->

            <!--        test div -->
           
  <!-- <div class="form-group ">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
  </div>
  <div class="form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button> -->


            <div id="option3" class="size_chart">
            <form method="get" action="{{ route('project.export') }}">
           <div class="form-group">
                   <label>Start Date:</label>
                   <div class="input-group mb-3">
                       <input class="form-control" type="date" id="start_date" name="start_date" width="276" required/>
                   </div>
               </div> 
               <div class="form-group">
                   <label>End Date:</label>
                   <div class="input-group mb-3">
                       <input class="form-control" type="date" id="end_date" name="end_date" width="276" required/>
                   </div>
               </div> 
            <div class="form-group">
            <label>Task Status:</label>
            <select class="form-control" id="task_status" name="task_status">
            <option value="" selected disabled hidden>Choose Status</option>
            <option value="Pending">Pending</option>
            <option value="In Progress">In Progress</option>
            <option value="Completed">Completed</option>
            
            </select>
                </div> 
             <div class="form-group">
                 <div class="input-group mb-3">
                     <button type="submit" class="btn btn-primary">Export By Status</button>
                 </div>
             </div>   
             
        </form>
            </div>
           
            
                
            
           

</div>
</div>
  

   
@endsection