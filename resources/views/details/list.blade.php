@extends('layouts.main')

@section('title')
   EOD Details
@endsection
 
@section('content')

<div class="container mt-3">
    @if ($message = session('success'))
    <div class="alert alert-success mx-1" role="alert">
        {{ $message }}
    </div>
     @endif
    <h2 class=" text-center">EOD Status</h2> 
        
            <!-- <form method="get" action="{{ route('project.export') }}">
                <div class="form-group">
                    <label>Date:</label>
                    <div class="input-group mb-3">
                        <input type="date" id="date" name="date" width="276" required/>
                    </div>
                </div> 
            
            <div class="form-group">
                    <label>Search Name</label>
                    <div class="input-group mb-3">
                        <input type="text" id="name"  name="name" width="276" required/>
                    </div>
                </div> 
            
            <div class="form-group">
                    <div class="input-group mb-3">
                        <button type="submit" class="btn btn-primary">Export By Name</button>
                    </div>
                </div>
            
            </form> -->
        </div> 

    @if(isset($users))    
    <div class="container mt-3">
    <div class="row">
    <form method="get" action="{{ route('list.search') }}">

    <div class="col form-group">
                   <label>Search(Projects/Tasks/Status):</label>
                   <input class="form-control" type="text" id="search" name="search" placeholder="search"/>
    </div> 
    <div class="col form-group">
                   <div class="input-group mb-3">
                       <button type="submit" class="btn btn-primary">Filter</button>
                   </div>
    </div>  
    </div>
    </form> 
    <form method="get" action="{{ route('project.list') }}">
           <div class="row date-group">
          
                 <div class="col form-group">
                   <label>Start Date:</label>
                   <div class="input-group mb-3">
                       <input type="date" id="start_date" name="start_date" width="276" required/>
                   </div>
               </div> 
               <div class="col form-group">
                   <label>End Date:</label>
                   <div class="input-group mb-3">
                       <input type="date" id="end_date" name="end_date" width="276" required/>
                   </div>
               </div> 
               <div class="col form-group export">
                   <div class="input-group mb-3">
                       <button type="submit" class="btn btn-primary">Export By Date</button>
                   </div>
               </div>   
               </div>
          </form>
         <!-- <a href="{{ route('project.list')}}" class="btn btn-primary" style="float: right">
             Export All
        </a> -->
    <table class="table table-hover mt-5">
        <thead>
            <tr>
                <th>Date</th>
                <th>Name</th>
                
                <th>Project Name</th>
                <th>Task Name</th>
                <th>Status</th>
                <th>Rating</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                  <tr>
                     
                      <td>{{ \Carbon\Carbon::parse($user->dateOfTheTask)->format('d/m/Y')}}</td>
                      <td>{{ $user->name }}</td>
                     
                      <td>{{ $user->project_name }}</td>
                      <td>{{ $user->taskName }}</td>
                      <td>{{ $user->projectStatus }}</td>
                      <td>{{ $user->rating }}</td>
                  </tr>
            @endforeach
        </tbody> 
   

    </table> 
    <div class="row">
    <div class="col-12">
        
    {{ $users->withQueryString()->links('pagination::bootstrap-4') }}
    </div>
	</div>

   </div>

</div>
@endif


@endsection