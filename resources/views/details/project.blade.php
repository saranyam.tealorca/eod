@extends('layouts.main')

@section('title')
   EOD Details
@endsection
 
@section('content')

<div class="container mt-3">
    @if ($message = session('success'))
    <div class="alert alert-success mx-1" role="alert">
        {{ $message }}
    </div>
     @endif
    <h2 class=" text-center">EOD Status</h2> 
        
            <!-- <form method="get" action="{{ route('project.export') }}">
                <div class="form-group">
                    <label>Date:</label>
                    <div class="input-group mb-3">
                        <input type="date" id="date" name="date" width="276" required/>
                    </div>
                </div> 
            
            <div class="form-group">
                    <label>Search Name</label>
                    <div class="input-group mb-3">
                        <input type="text" id="name"  name="name" width="276" required/>
                    </div>
                </div> 
            
            <div class="form-group">
                    <div class="input-group mb-3">
                        <button type="submit" class="btn btn-primary">Export By Name</button>
                    </div>
                </div>
            
            </form> -->
        </div> 

    @if(isset($users))    
    <div class="container mt-3">
         <!-- <a href="{{ route('project.profile', $nickname)}}" class="btn btn-primary" style="float: right">
             Export All
        </a> -->
    <table class="table table-hover mt-5">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Date</th>
                <th>Project Name</th>
                <th>Task Name</th>
                <th>Status</th>
                <th>Rating</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                  <tr>
                      <td>{{ ++$i }}</td>
                      <td>{{ $user->name }}</td>
                      <td>{{ $user->dateOfTheTask }}</td>
                      <td>{{ $user->project_name }}</td>
                      <td>{{ $user->taskName }}</td>
                      <td>{{ $user->projectStatus }}</td>
                      <td>{{ $user->rating }}</td>
                  </tr>
            @endforeach
        </tbody> 
    </table> 
   </div>
</div>
@endif
@endsection