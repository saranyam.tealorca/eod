<?php

namespace App\Exports;

use App\Models\Details;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;
class DetailsExport implements FromCollection,WithHeadings
{

    //Excel heading added here
    public function headings():array{
        return[
            'Id',
            'Name',
            'Date',
            'Project Name',
            'Task Name',
            'Status',
            'Rating',
            
        ];
    } 


    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {  
        
        $project_id = request()->input('project_id');
        $user_name = request()->input('user_name');
        $start_date = request()->input('start_date');
        $end_date = request()->input('end_date');
        $task_status = request()->input('task_status');
        //$users = request() ;
        //dd($request);
        //return Details::all();
        // $name = request()->input('name') ;
        // $date = request()->input('date') ;
         if(isset($project_id)){
            $users = DB::table('task')
            ->join('project', 'project.id', '=', 'task.project_id')
            
            ->select('task.id', 'task.name' ,'task.dateOfTheTask','project.project_name', 'task.taskName','task.projectStatus','task.rating')
            ->where('task.project_id', $project_id)
           
            ->whereBetween('dateOfTheTask', [ $start_date, $end_date ] )
            
            ->orderBy('task.id', 'DESC')
            // ->groupBy('orders.order_number','products.title','orders.order_number','order_items.total_price')
            ->get();
         }elseif(isset($user_name)){
            $users = DB::table('task')
            ->join('project', 'project.id', '=', 'task.project_id')
            
            ->select('task.id', 'task.name' ,'task.dateOfTheTask','project.project_name', 'task.taskName','task.projectStatus','task.rating')
            ->where('task.name', $user_name)
            ->whereBetween('dateOfTheTask', [ $start_date, $end_date ] )
            ->orderBy('task.id', 'DESC')
            // ->groupBy('orders.order_number','products.title','orders.order_number','order_items.total_price')
            ->get();
         }
         elseif(isset($start_date) && isset($end_date)){
            $users = DB::table('task')
            ->join('project', 'project.id', '=', 'task.project_id')
            
            ->select('task.id', 'task.name' ,'task.dateOfTheTask','project.project_name', 'task.taskName','task.projectStatus','task.rating')
            ->whereBetween('dateOfTheTask', [ $start_date, $end_date ] )
            
            ->orderBy('task.id', 'DESC')
            // ->groupBy('orders.order_number','products.title','orders.order_number','order_items.total_price')
            ->get();
         }
         elseif(isset($task_status)){
            $users = DB::table('task')
            ->join('project', 'project.id', '=', 'task.project_id')
            
            ->select('task.id', 'task.name' ,'task.dateOfTheTask','project.project_name', 'task.taskName','task.projectStatus','task.rating')
            ->where('task.projectStatus', 'LIKE', "%{$task_status}%")
            ->whereBetween('dateOfTheTask', [ $start_date, $end_date ] )
            ->orderBy('task.id', 'DESC')
            // ->groupBy('orders.order_number','products.title','orders.order_number','order_items.total_price')
            ->get();
         }
         else{
        $users = DB::table('task')
            ->join('project', 'project.id', '=', 'task.project_id')
            
            ->select('task.id', 'task.name' ,'task.dateOfTheTask','project.project_name', 'task.taskName','task.projectStatus','task.rating')
            ->orderBy('task.id', 'DESC')
            // ->groupBy('orders.order_number','products.title','orders.order_number','order_items.total_price')
            ->get(20);
         }
            return $users;
    }


    // public static function getuserData(){
    //      $users = DB::table('task')
    //         ->join('project', 'project.id', '=', 'task.project_id')
            
    //         ->select('task.id', 'task.name' ,'task.dateOfTheTask','project.projectName', 'task.taskName','task.projectStatus','task.rating')
    //         // ->groupBy('orders.order_number','products.title','orders.order_number','order_items.total_price')
    //         ->get();
    //     return $users;
    //   }
}
