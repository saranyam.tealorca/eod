<?php

namespace App\Exports;

use App\Models\Eod;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;

class EodExport implements FromView, ShouldAutoSize, WithEvents
{
    /**
     * @return View
     */
    public function view(): View
    {
        $users = DB::table('task')
            ->join('project', 'project.id', '=', 'task.project_id')
            
            ->select('task.id', 'task.name' ,'task.dateOfTheTask','project.projectName', 'task.taskName','task.projectStatus','task.rating')
            // ->groupBy('orders.order_number','products.title','orders.order_number','order_items.total_price')
            ->get();
        return view('details.list', compact('users'));
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getDelegate()->setRightToLeft(true);
            },
        ];
    }
}
// use Maatwebsite\Excel\Concerns\FromCollection;

// class EodExport implements FromCollection
// {
//     /**
//     * @return \Illuminate\Support\Collection
//     */
//     public function collection()
//     {
//         return Eod::all();
//     }
// }
