<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\DetailsExport;
use App\Exports\EodExport;
use App\Models\Project;
use App\Models\Employee;
use App\Models\Task;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
class ProjectController extends Controller
{

    //list of project
    public function index(Request $request)
    {
      $project = Project::paginate(5);
      return view('project.index',compact('project'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

   //create a new project
    public function create()
      {
           return view('project.create');
      } 


    //store the project
    public function store(Request $request){
       //dd($request);
      $request->validate([
        'project_name' => 'required',
        
    ]);

    // if($request->id){
    //     $project= Project::find($request->id);
    //  //    $emp->gender=$request->gender;
    //     //dd($emp->gender);
    //  }else{
    //      $project=new Project();
    //  }
     $project = (isset($request->id)) ? Project::find($request->id) : new Project();
     $project->project_name=$request->project_name;
     $project->save();
      //dd($project);
      return redirect()->route('project.index')
      ->with('success','project created successfully.');
      
    }
    //edit project
    public function edit($id)
    {
        $project =Project::find($id);
        return view('project.create',compact('project'));
    }
    //delete Project
    public function destroy($id)
    {
        $project =Project::find($id);
        $project->delete();
        return back();
    }

    //get api
    public function getApi(Request $request){


        /* -------------------------------- TODO: Logic -------------------------------- */
        $responce['success'] = false;
        $responce['message'] = 'Successfully';
        $responce['data'] = [];
        
        return response()->json($responce,200);
    }


    //postapi eod status code
    public function postApi(Request $request){

      $request->validate([
        'taskName' => 'required',
        'projectStatus' => 'required',
        'name' => 'required',
        'dateOfTheTask' => 'required',
        'rating' => 'required',
      

    ]);
        $responce['success'] = false;
        $responce['message'] = 'unable to save';
        $task=new Task();
     
       
        //relation one to one
       //to do
       //project_id
     
         $project_id = Project::where('project_name',$request->projectName)->pluck('id')->first();
         //dd($project_id);
         if($project_id){
            $task->extraParam=json_encode($request->all());
            $task->projectStatus=$request->projectStatus;
            $task->taskName=$request->taskName;
            $task->name=$request->name;
            $task->dateOfTheTask=$request->dateOfTheTask;
            $task->project_id=$project_id;
            $task->rating=$request->rating;
           //$task->project_id=$request->id;
       
            $task->save();
            $responce['data'] = $task;
            $responce['success'] = true;
            $responce['message'] = 'Successfully created';
         }

        
        
        return response()->json($responce,200);
      
    }


    //Get project json
    public function getJson(){

        $projects = json_decode(file_get_contents(storage_path() . "/projects.json"), true);
        //print_r($projects);
        foreach($projects as $project){
           //dd($project['value']);
           $res = new Project;
           $res->projectName = $project['value'];
           //$res->extraParam = $projects; 
           //echo $res;
           //$res->save();

        }
    }

  //dropdowm project api
   public function getProject(Request $request){
        //    $projects = Project::all();
       $projects = Project::select('id','project_name')->get()->toArray();
       //$projects = Project::select('id','projectName')->get();
       //dd($projects);
        $responce['success'] = true;
        $responce['message'] = 'Successfully';
        $responce['project'] = $projects;
        
         return response()->json($responce,200);
        

   }


     //dropdowm project list
     public function projectList(Request $request){
      //    $projects = Project::all();
     $projects = Project::select('id','project_name')->get();
     $employees = Employee::select('id','name')->get();
     return view('details.filter',compact('projects','employees'));
    }
    
   

   //export excel file
   public function get_details(Request $request)
   {

    //dd($request->all());
    // $request->validate([
    //     'name' => 'required',
    //     'date' => 'required',
    // ]);

     $date1=date('Y-m-d',strtotime($request->start_date));
     $date2=date('Y-m-d',strtotime($request->end_date));
     $request->start_date = $date1;
     $request->end_date = $date2;
    // //dd($request->all());
    $user = New DetailsExport();
    $users = $user->collection();
    //dd($users);
    foreach($users as $user){
      //print_r($user->dateOfTheTask);
      
      if($user->project_name) {
        //print_r($user);
         //$use[]=$user;
        
         return Excel::download(new DetailsExport, 'details.xlsx');
        //  return view('details.project',compact('users'))
        //  ->with('i', (request()->input('page', 1) - 1) * 5);
      }
      elseif($user->user_name) {
        // return view('details.project',compact('users'))
        // ->with('i', (request()->input('page', 1) - 1) * 5);
        return Excel::download(new DetailsExport, 'details.xlsx');
      }
      // elseif($user->start_date && $user->end_date) {
      //   // return view('details.project',compact('users'))
      //   // ->with('i', (request()->input('page', 1) - 1) * 5);
      //   return Excel::download(new DetailsExport, 'details.xlsx');
      // }
      elseif($user->task_status) {
        // return view('details.project',compact('users'))
        // ->with('i', (request()->input('page', 1) - 1) * 5);
        return Excel::download(new DetailsExport, 'details.xlsx');
      }
      else{
        return view('details.project',compact('users'))
        ->with('i', (request()->input('page', 1) - 1) * 5);
      }
    }
    //return Excel::download($users, 'details.xlsx');
    //dd($users);
    
       // return Excel::download(new DetailsExport, 'details.xlsx');
      //  return view('details.list',compact('users'))
      //  ->with('i', (request()->input('page', 1) - 1) * 5);
    
    return back()->withSuccess('Opps! No data');
    
       
}
     //search project_name
     public function searchList(Request $request)
     {
      //dd($request->all());
      $request=request('search');
      $user = New DetailsExport();
      $query = $user->collection();
      //dd($request->search);
      //dd($query);
    
    //  foreach ( $users as $query)
    //  {
    //       //dd($query->project_name);
    //  }
      
    
     //dd($request->project_name);
      //$query->when( ( isset($request->search)), function ($query) use ($request){
        
        // if(!empty($query)){
        //     foreach ($query as $key => $value) {
        //         $query[] = $value;
        //     }
        // }
        //dd($request->project_name);
           //$query[]=$query->where('project_name','Like', $request->search)->orWhere('taskName','Like', $request->search);
                //$query[] = $query->where('project_name','LIKE', "%{$request->project_name}%");
                $query[] = $query->filter(function ($users) {
                  //dd(request('search'));
                  return $users->project_name == request('search') || $users->taskName == request('search') || $users->projectStatus == request('search');
              });
             
                // $query[]=$query->where('project_name', $request->search);
               
                //return $users;
                //return view('details.list',compact('users'));
            //});
            //dd($query->last());
            $users=$query->last();
            $totalGroup = count($users);
            $perPage = 10;
            $page = Paginator::resolveCurrentPage('page');
            $users = new LengthAwarePaginator($users->forPage($page, $perPage), $totalGroup, $perPage, $page, [
                'path' => Paginator::resolveCurrentPath(),
                'pageName' => 'page',
            ]);
            return view('details.list',compact('users'));
     }
    
   //export details date start and end list
     public function list(Request $request)
     {
       //dd($request->all());
      $date1=date('Y-m-d',strtotime($request->start_date));
      $date2=date('Y-m-d',strtotime($request->end_date));
      $request->start_date = $date1;
      $request->end_date = $date2;
      $user = New DetailsExport();
      $users = $user->collection();
      //dd($users);
      foreach($users as $user){
      if($users) {
          // return view('details.project',compact('users'))
          // ->with('i', (request()->input('page', 1) - 1) * 5);
          return Excel::download(new DetailsExport, 'details.xlsx');
        }
      }
      //return Excel::download(new DetailsExport, 'details.xlsx');
      
        //$details = new DetailsExport();
      
         //dd($userData);
        // $users = DB::table('task')
        // ->join('project', 'project.id', '=', 'task.project_id')
        
        // ->select('task.id', 'task.name' ,'task.dateOfTheTask','project.projectName', 'task.taskName','task.projectStatus','task.rating')
        // // ->groupBy('orders.order_number','products.title','orders.order_number','order_items.total_price')
        // ->get();
        //dd($users);
        //$task = Task::paginate(5);  
        //  $request=$users; 
        // $detail_export = new EodExport();
        // $detail_export->view($request);    
        //return view('details.list',compact('users'));
     }


     public function filter($serializeArray)
{

  $user = New DetailsExport();
  $users = $user->collection();
    //dd($serializeArray);
    foreach($users as $user){
      //print_r($user->dateOfTheTask);
      if($user->project_name) {
        //print_r($user);
         //$use[]=$user;
        
  
        
      }
      elseif($user->user_name) {
        
        return Excel::download(new DetailsExport, 'details.xlsx');
      }elseif($user->start_date && $user->end_date) {
       
        return Excel::download(new DetailsExport, 'details.xlsx');
      }
      elseif($user->task_status) {
        
        return Excel::download(new DetailsExport, 'details.xlsx');
      }
      else{
        return Excel::download(new DetailsExport, 'details.xlsx');
      }
    }
     
}

}
