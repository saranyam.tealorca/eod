<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
class UserController extends Controller
{

    //list of project
    public function index(Request $request)
    {
      $user = Employee::paginate(5);
      

        // $query->when( ( isset($request->q)), function ($query) use ($request){

        //     $query = $query->where('name', 'LIKE', "%{$request->q}%") 
        //            ->orWhere('created_at', 'LIKE', "%{$request->q}%");

        //     return $query;
        // });

        // $limit = 20;
        // $user = $query->paginate($limit);
        return view('users.index',compact('user'));
        // $project = Project::latest()->paginate(5);

        // return view('project.index',compact('project'))
        //     ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    //create a new user
    public function create()
      {
           return view('users.create');
      } 
      //store the users
    public function store(Request $request){
        //dd($request);
       $request->validate([
         'user_name' => 'required',
         'email' => 'required',
         
     ]);
 
     // if($request->id){
     //     $project= Project::find($request->id);
     //  //    $emp->gender=$request->gender;
     //     //dd($emp->gender);
     //  }else{
     //      $project=new Project();
     //  }
     //dd($request);
      $user = (isset($request->id)) ? Employee::find($request->id) : new Employee();
      $user->name=$request->user_name;
      $user->email=$request->email;
      $user->save();
       //dd($user);
       return redirect()->route('user.index')
       ->with('success','User created successfully.');
       
     }
     //edit project
    public function edit($id)
    {
        $user =Employee::find($id);
        return view('users.create',compact('user'));
    }
    //delete Project
    public function destroy($id)
    {
        $user =Employee::find($id);
        $user->delete();
        return back();
    }
     //dropdowm users api
   public function getUsers(Request $request){
    $responce['success'] = false;
    $responce['message'] = 'unable to save';
    //    $projects = Project::all();
   $users = Employee::select('id','name')->get()->toArray();
   //$projects = Project::select('id','projectName')->get();
   //dd($projects);
    $responce['success'] = true;
    $responce['message'] = 'Successfully';
    $responce['users'] = $users;
    
     return response()->json($responce,200);
    

}
}
