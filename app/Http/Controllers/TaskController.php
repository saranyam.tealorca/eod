<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\DetailsExport;
use Illuminate\Pagination\LengthAwarePaginator;
class TaskController extends Controller
{
    public function getApi(Request $request){


        /* -------------------------------- TODO: Logic -------------------------------- */
        $responce['success'] = false;
        $responce['message'] = 'Successfully';
        $responce['data'] = [];
        
        return response()->json($responce,200);
    }

    public function postApi(Request $request){
        dd($request->all());
        // $request->validate([
        //     'projectName' => 'required',
           
        // ]);
        // $project=new Project();
        // $project->projectName=$request->projectName;
        
        // $responce['success'] = false;
        // $responce['message'] = 'Successfully';
        // $responce['data'] = $project;
        
        // return response()->json($responce,200);
        //$project->extraParam=$request->extraParam;
        //dd($project);
    }


    //list of project
    public function index(Request $request)
    {
      //$project = Project::paginate(5);
      $query = DetailsExport::collection();
      $users = [];
      if(!empty($query)){
          foreach ($query as $key => $value) {
              $users[] = $value;
          }
      }
    
      $paginate = 5;
      $page = $query->get('page', 1);
      
  //dd($page);
      $offSet = ($page * $paginate) - $paginate;  
      $itemsForCurrentPage = array_slice($users, $offSet, $paginate, true);  
      $users = new LengthAwarePaginator($itemsForCurrentPage, count($users), $paginate, $page);
      return view('details.list',compact('users'))->with('i');
    }
}
