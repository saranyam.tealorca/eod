<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\User;
use App\Exports\DetailsExport;
use Hash;
class AdminController extends Controller
{

   //login blade page
    public function index()
    {
        return view('admin.login');
    } 

    //admin authenticated login
    public function adminLogin(Request $request){
       
//                  return User::create([
//             'name' => 'admin',
//             'email' => 'admin@gmail.com',
//             'password' => Hash::make('admin')
//           ]);
// dd($request);

        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
     

       
          $credentials = $request->only('email', 'password');
         
         
             //$req=Auth::attempt($credentials);
             if (Auth::attempt($credentials)) {
           
           
            return redirect()->intended('dashboard')
            ->withSuccess('You have Successfully logged in');
             }

           
  
         return redirect("login")->withSuccess('Sorry! You have entered invalid credentials');
    }

    public function dashboard()
    {
        // $ref=Auth::check();
        // dd($ref);
        if(Auth::check()){
            $user = New DetailsExport();
            $users = $user->collection();
            $totalGroup = count($users);
            $perPage = 10;
            $page = Paginator::resolveCurrentPage('page');
            $users = new LengthAwarePaginator($users->forPage($page, $perPage), $totalGroup, $perPage, $page, [
                'path' => Paginator::resolveCurrentPath(),
                'pageName' => 'page',
            ]);

            //dd($users);
            // $users->paginate(10)
            // ->appends(withQueryString());
            
            //$users =DetailsExport::collection();
            
            // $query->when( ( isset($request->q)), function ($query) use ($request){

            //     $query = $query->where('project_name', 'LIKE', "%{$request->q}%") 
            //            ->orWhere('created_at', 'LIKE', "%{$request->q}%");
    
            //     return $query;
            // });

            // $limit = 10;
            // $users = $query->paginate($limit);
            //$users = $this->paginate($query);
            //dd($users);
            //return view('paginate', compact('data'));
            //Products::paginate(20);
           
            // $users = [];
            // if(!empty($query)){
            //     foreach ($query as $key => $value) {
            //         $users[] = $value;
            //     }
            // }
            // $total = count($users);
            // $perPage = 5; // How many items do you want to display.
            // $currentPage = 1; // The index page.
            // $paginator = new LengthAwarePaginator($users, $total, $perPage, $currentPage);
            //dd($total);
            // $paginate = 20;
            // $page = $query->get('page', 1);
            // $page = $query->paginate(10);
            
        //dd($page);
            // $offSet = ($page * $paginate) - $paginate;  
            // $itemsForCurrentPage = array_slice($users, $offSet, $paginate, true);  
            // $users = new LengthAwarePaginator($itemsForCurrentPage, count($users), $paginate, $page);
            //$users = $users->toArray();
           //dd($users);

            // return view('details.list',compact('users'))->with('i');
            return view('details.list',compact('users'));
        }
  
        return redirect("login")->withSuccess('Opps! You do not have access');
    }


    // public function paginate($items, $perPage = 10, $page = null, $options = [])
    // {
    //     $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
    //     $items = $items instanceof Collection ? $items : Collection::make($items);
    //     return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    // }

   //logout session
    public function logout() {
        Session::flush();
        Auth::logout();
  
        return Redirect('login');
    }
}
